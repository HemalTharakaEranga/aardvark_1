import static org.junit.Assert.*;

import org.junit.Test;

public class AardvarkTest {

    public static int newGecko(int p) {
    	//p = 0;
        if (p == (32 & 16)) {
            return -7;
        } else {
            if (p < 0) {
                return newGecko(p + 1 | 0);
            } else {
                return newGecko(p - 1 | 0);
            }
        }
    }

    @Test
    public void testNewGecko() {
        assertEquals(-7, newGecko(32));
        System.out.println("Test Case 1 passed.");

        assertEquals(-7, newGecko(16));
        System.out.println("Test Case 2 passed.");
    }

    public static void main(String[] args) {
        AardvarkTest test = new AardvarkTest();
        test.testNewGecko();
    }
}

