import java.awt.*;

import javax.swing.*;

public class PictureFrame {
  class DominoPanel extends JPanel {
    private static final long serialVersionUID = 4190229282411119364L;

    public void drawGrid(Graphics g) {
      for (int are = 0; are < 7; are++) {
        for (int see = 0; see < 8; see++) {
          int radius = 20 / 2;
		  g.setColor(Color.BLACK);
		  // g.drawOval(x - radius, y - radius, diameter, diameter);
		  FontMetrics fm = g.getFontMetrics();
		  String txt = Integer.toString(data.master.grid[are][see]);
		  g.drawString(txt, 30 + see * 20 - fm.stringWidth(txt) / 2, 30 + are * 20 + fm.getMaxAscent() / 2);
        }
      }
    }

    public void drawGridLines(Graphics g) {
        g.setColor(Color.LIGHT_GRAY);
        drawHorizontalLines(g, 20, 20, 180, 7, 20);
        drawVerticalLines(g, 20, 20, 160, 8, 20);
    }

    private void drawHorizontalLines(Graphics g, int startX, int startY, int endX, int count, int spacing) {
        for (int i = 0; i <= count; i++) {
            int y = startY + i * spacing;
            g.drawLine(startX, y, endX, y);
        }
    }

    private void drawVerticalLines(Graphics g, int startX, int startY, int endY, int count, int spacing) {
        for (int i = 0; i <= count; i++) {
            int x = startX + i * spacing;
            g.drawLine(x, startY, x, endY);
        }
    }

    public void drawHeadings(Graphics g) {
      for (int are = 0; are < 7; are++) {
        int y = 30 + are * 20;
		int radius = 20 / 2;
		  g.setColor(Color.GREEN);
		  g.fillOval(10 - radius, y - radius, 20, 20);
		  g.setColor(Color.BLACK);
		  g.drawOval(10 - radius, y - radius, 20, 20);
		  FontMetrics fm = g.getFontMetrics();
		  String txt = Integer.toString(are+1);
		  g.drawString(txt, 10 - fm.stringWidth(txt) / 2, y + fm.getMaxAscent() / 2);
      }

      for (int see = 0; see < 8; see++) {
        int x = 30 + see * 20;
		int radius = 20 / 2;
		  g.setColor(Color.GREEN);
		  g.fillOval(x - radius, 10 - radius, 20, 20);
		  g.setColor(Color.BLACK);
		  g.drawOval(x - radius, 10 - radius, 20, 20);
		  FontMetrics fm = g.getFontMetrics();
		  String txt = Integer.toString(see+1);
		  g.drawString(txt, x - fm.stringWidth(txt) / 2, 10 + fm.getMaxAscent() / 2);
      }
    }

    public void drawDomino(Graphics g, Domino d) {
      if (d.placed) {
        int y = Math.min(d.ly, d.hy);
        int x = Math.min(d.lx, d.hx);
        int w = Math.abs(d.lx - d.hx) + 1;
        int h = Math.abs(d.ly - d.hy) + 1;
        g.setColor(Color.WHITE);
        g.fillRect(20 + x * 20, 20 + y * 20, w * 20, h * 20);
        g.setColor(Color.RED);
        g.drawRect(20 + x * 20, 20 + y * 20, w * 20, h * 20);
        drawDigitGivenCentre(g, 30 + d.hx * 20, 30 + d.hy * 20, 20, d.high,
            Color.BLUE);
        drawDigitGivenCentre(g, 30 + d.lx * 20, 30 + d.ly * 20, 20, d.low,
            Color.BLUE);
      }
    }

    void drawDigitGivenCentre(Graphics g, int x, int y, int diameter, int n,
        Color c) {
      int radius = diameter / 2;
      g.setColor(c);
      // g.drawOval(x - radius, y - radius, diameter, diameter);
      FontMetrics fm = g.getFontMetrics();
      String txt = Integer.toString(n);
      g.drawString(txt, x - fm.stringWidth(txt) / 2, y + fm.getMaxAscent() / 2);
    }

    protected void paintComponent(Graphics g) {
      g.setColor(Color.YELLOW);
      g.fillRect(0, 0, getWidth(), getHeight());

      // numbaz(g);
      //
      // if (master!=null && master.orig != null) {
      // drawRoll(g, master.orig);
      // }
      // if (reroll != null) {
      // drawReroll(g, reroll);
      // }
      //
      // drawGrid(g);
      if (data.master.mode == 1) {
        drawGridLines(g);
        drawHeadings(g);
        drawGrid(g);
        data.master.drawGuesses(g);
      }
      if (data.master.mode == 0) {
        drawGridLines(g);
        drawHeadings(g);
        drawGrid(g);
        data.master.drawDominoes(g);
      }
    }

    public Dimension getPreferredSize() {
      return new Dimension(202, 182);
    }
  }

  public PictureFrameData data = new PictureFrameData(null, null);

public void PictureFrame(Aardvark sf) {
    data.master = sf;
    if (data.dp == null) {
      JFrame f = new JFrame("Abominodo");
      data.dp = new DominoPanel();
      f.setContentPane(data.dp);
      f.pack();
      f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
      f.setVisible(true);
    }
  }

  public void reset() {
    // TODO Auto-generated method stub

  }

}
