
public class PictureFrameData {
	public int[] reroll;
	public Aardvark master;
	public PictureFrame.DominoPanel dp;

	public PictureFrameData(int[] reroll, Aardvark master) {
		this.reroll = reroll;
		this.master = master;
	}
}