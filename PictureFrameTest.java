import static org.junit.Assert.*;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import org.junit.Test;

public class PictureFrameTest {

    @Test
    public void testFillDigitGivenCentre() {
       
        BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
        Graphics g = image.getGraphics();
        fillDigitGivenCentre(g, 10, 30, 20, 1);
        FontRenderContext frc = g.getFontMetrics().getFontRenderContext();
        TextLayout layout = new TextLayout("1", g.getFont(), frc);
        Rectangle2D bounds = layout.getBounds();

        // Define the expected bounds.
        Rectangle expectedBounds = new Rectangle(1, -9, 4, 9);

        // Assert that the digit is drawn correctly.
        assertEquals(expectedBounds, bounds.getBounds());
        
        // Store the output value for further analysis or logging.
        System.out.println("Actual Bounds: " + bounds.getBounds());
        System.out.println("Expected Bounds: " + expectedBounds);
    }

    public void fillDigitGivenCentre(Graphics g, int i, int j, int k, int l) {

        int width = 20;
        int height = 20;
        g.setColor(Color.black);
        g.fillRect(i - width / 2, j - height / 2, width, height);
        g.setColor(Color.white);
        g.drawString(String.valueOf(l), i - width / 2 + 5, j - height / 2 + 10);
    }
}
